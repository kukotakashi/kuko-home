package kuko.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KukoHomeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KukoHomeApiApplication.class, args);
	}
}
