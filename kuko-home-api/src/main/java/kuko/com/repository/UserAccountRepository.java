package kuko.com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kuko.com.entity.UserAccount;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

}
